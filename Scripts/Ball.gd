extends KinematicBody2D

onready var _vaus = preload("res://Scenes/Vaus.tscn")

var speed: int = 200 # vitesse de la balle
var speedFactor: float = 1.0 # facteur de ralentissement
var velocity: Vector2 = Vector2.ZERO
onready var vaus = get_node("../../") # Noeud Vaus
var num_round: int = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var dir : Vector2 = Vector2(rand_range(-1,1),-1) # Départ de la balle
	velocity = dir * speed
	randomize()
	
func _draw() -> void:
	num_round = get_node("/root/Game").ROUND
	print("Speed ball: ", speed, " Factor: ", speedFactor)

func is_bonus() -> bool:
	var _tilemap = get_node("/root/Game/Levels/Level"+str(num_round).pad_zeros(2))
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta: float) -> void:
	if not vaus.goBall: # On ne déplace pas la balle
		position.x = vaus.position.x
	else:
		# On déplace la balle suivant l'algorithme
		var collider = move_and_collide(velocity*delta)
		if collider:
			velocity = velocity.bounce(collider.normal) * speedFactor # Rebond
			# Ajoute de l'angle aléatoirement
			velocity = Vector2(rand_range(velocity.x-15, velocity.x+15),velocity.y) 
			# Si collision avec une brique, on la détruit
			var item = collider.collider
			if item.is_in_group("Bricks"):
				item.queue_free()
				get_node("/root/Game").nb_brick -= 1
				# On compte les points
				var score = int(get_node("/root/Game/HUD/Score").text)
				score += int(item.editor_description)
				get_node("/root/Game/HUD/Score").text = str(score).pad_zeros(6)

			elif item.is_in_group("BrickGrey"):
				# Joue l'animation du sprite
				var _spr = item.get_node("AnimatedSprite")
				_spr.play("default")
				_spr.frame = 0
				item.hit -=1
				print("HIT: ",item.hit)
				if item.hit == 0:
					item.queue_free()
					get_node("/root/Game").nb_brick -= 1
					# On compte les points
					var score = int(get_node("/root/Game/HUD/Score").text)
					# 50 x rounded
					score += 50*num_round
					get_node("/root/Game/HUD/Score").text = str(score).pad_zeros(6)
			elif item.is_in_group("BrickGold"):
				# Joue l'animation du sprite
				var _spr = item.get_node("AnimatedSprite")
				_spr.play("default")
				_spr.frame = 0
			print("Nb bricks: ",get_node("/root/Game").nb_brick)

			# Dernière brique
			if get_node("/root/Game").nb_brick == 0:
#				vaus.doMove = false
				get_node("/root/Game").nextLevel = true
				# Joue un son
				# Supprime le level
				get_node("/root/Game/Levels/Level"+str(num_round).pad_zeros(2)).queue_free()
				# Supprime la raquette
				get_node("/root/Game/Vaus").queue_free()
