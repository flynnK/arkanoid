extends Node2D

var lostBall: bool = false # Flag de la perte d'une balle
var nb_brick : int = 0 # Nombre de briques dans le niveau
var ROUND: int = 0
var nextLevel: bool = false # Flag pour instancier le niveau suivant

func _input(event: InputEvent) -> void:
	var _fullscreen = event.is_action_pressed("key_f")
	var _esc = event.is_action_pressed("ui_cancel")
	
	if _fullscreen:
		OS.window_fullscreen = !OS.window_fullscreen
	# TODO: penser à supprimer cette sortie de programme. cela doit se faire à partir du menu
	if _esc:
		get_tree().quit(0) 

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var level	= load("res://Scenes/Levels/Level01.tscn").instance()
	var vaus	= load("res://Scenes/Vaus.tscn").instance()
	add_child(vaus)
	$Levels.add_child(level)

func _process(_delta: float) -> void:
	if lostBall:
		$Vaus.free() # Ne pas utiliser queue_free dans ce cas précis.
		lostBall = false
		var vaus = load("res://Scenes/Vaus.tscn").instance()
		get_node("/root/Game").call_deferred("add_child", vaus)
	# Niveau suivant ?
	if nextLevel:
		if ROUND == 36: # Dernier level
			print("tu es un winner :)")
			# TODO à finir
			get_tree().quit(0)

		else:
			ROUND+=1
			print("Load level"+str(ROUND).pad_zeros(2)+".tscn")
			# On instancie le prochain niveau
			var level = load("res://Scenes/Levels/Level"+str(ROUND).pad_zeros(2)+".tscn").instance()
			get_node("/root/Game/Levels/").call_deferred("add_child", level)
			# On instancie la raquette
			var vaus = load("res://Scenes/Vaus.tscn").instance()
			get_node("/root/Game").call_deferred("add_child", vaus)
			nextLevel = false
