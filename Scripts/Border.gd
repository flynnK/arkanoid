extends TileMap

func _on_Area2D_body_entered(body: Node) -> void:
	if body.name == "Ball":
		# S'il y a plusieurs balles, on supprime celle qui sort
		if body.get_parent().get_child_count() > 1:
			body.queue_free()
		# sinon on supprime le noeud de la raquette pour relancer le niveau
		else:
			print(get_node("/root/Game/").get_children())
			get_node("/root/Game/Vaus").doMove = false # La raquette ne bouge plus
			get_node("/root/Game/Vaus/Sprite").play("Die")
			body.queue_free() # Supprime la balle
			
			var live = get_node("/root/Game/HUD/Lives")
			# Supprime toujours la dernière raquette
			if live.get_child_count() >0 :
				live.get_children()[-1].queue_free()
#				get_node("/root/Game").lostBall = true # Pour réinstancier la raquette
			else:
				print("GAME OVER")
				pass # GAME OVER
