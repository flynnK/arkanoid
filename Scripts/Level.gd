extends Node2D

# Les briques
onready var _brickWhite		= preload("res://Scenes/Bricks/50.tscn")
onready var _brickOrange	= preload("res://Scenes/Bricks/60.tscn")
onready var _brickCyan		= preload("res://Scenes/Bricks/70.tscn")
onready var _brickGreen		= preload("res://Scenes/Bricks/80.tscn")
onready var _brickRed		= preload("res://Scenes/Bricks/90.tscn")
onready var _brickBlue		= preload("res://Scenes/Bricks/100.tscn")
onready var _brickPink		= preload("res://Scenes/Bricks/110.tscn")
onready var _brickYellow	= preload("res://Scenes/Bricks/120.tscn")
onready var _brickGold		= preload("res://Scenes/Bricks/Gold.tscn")
onready var _brickGrey		= preload("res://Scenes/Bricks/Grey.tscn")
# Les bonus
onready var _bonusS	= preload("res://Scenes/Bonus/BonusS.tscn")
onready var _bonusC	= preload("res://Scenes/Bonus/BonusC.tscn")
onready var _bonusL	= preload("res://Scenes/Bonus/BonusL.tscn")
onready var _bonusE	= preload("res://Scenes/Bonus/BonusE.tscn")
onready var _bonusD	= preload("res://Scenes/Bonus/BonusD.tscn")
onready var _bonusB	= preload("res://Scenes/Bonus/BonusB.tscn")
onready var _bonusP	= preload("res://Scenes/Bonus/BonusP.tscn")
# Table des emplacements de bonus
var bonus: Array = []
const NB_BONUS = 7

func rand_bonus() -> void:
	randomize()
	# Rempli aléatoirement les bonus là où il y a des briques
	var bonus_type: Array = [_bonusS, _bonusC, _bonusL, _bonusE, _bonusD, _bonusB, _bonusP]
	for cell in $Bricks.get_used_cells():
		var rnd_bonus = randi() %100
		if rnd_bonus >= 88:
			bonus.append(bonus_type[randi()%NB_BONUS].instance()) # Ajoute le type de bonus
		else:
			bonus.append(null) # Pas de bonus par défaut
	for item in bonus:
		if item:
			print("Bonus: ", item.name)
		else:
			print("Bonus: ",item)


func _draw() -> void:
	# Affiche le level en cours
	get_node("Border/Round").text+=str(get_node("/root/Game").ROUND)
	yield(get_tree().create_timer(1.5), "timeout")
	get_node("Border/Round").hide()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Génère le tableau des bonus
	rand_bonus()
	
	# récupère le numéro de level pour le mettre dans le round
	var regex = RegEx.new()
	regex.compile("[0-9]+")
	get_node("/root/Game").ROUND = int(regex.search(name).get_string())
	print("Round: ", get_node("/root/Game").ROUND)
	
	for cell in $Bricks.get_used_cells():
		# Récupère la position en pixel en fonction de la case de la tilemap
		var pos : Vector2 = $Bricks.map_to_world(cell)+Vector2(8,20) # Rattrape le décallage
		# Instancie la bonne brique en fonction de sa couleur
		var brick : StaticBody2D
		# Récupère l'ID du tile pour récupérer son nom
		var id : String = $Bricks.tile_set.tile_get_name($Bricks.get_cellv(cell))
		match id:
			"50": # Blanc
				brick = _brickWhite.instance()
			"60": # Orange
				brick = _brickOrange.instance()
			"70": # Cyan
				brick = _brickCyan.instance()
			"80": # Green
				brick = _brickGreen.instance()
			"90": # Red
				brick = _brickRed.instance()
			"100": # Blue
				brick = _brickBlue.instance()
			"110": # Pink
				brick = _brickPink.instance()
			"120": # Yellow
				brick = _brickYellow.instance()
			"Gold": # Indestructible
				brick = _brickGold.instance()
			"Grey": # 50 x le level
				brick = _brickGrey.instance()
				
		$AllBricks.add_child(brick)
		brick.position = pos # Position du sprite = position du tile
		
	get_node("/root/Game").nb_brick = $AllBricks.get_child_count() # Récupère le nombre de briques du niveau
	get_node("/root/Game").nb_brick = 2
	print("Nb brick: ",get_node("/root/Game").nb_brick)
	$Bricks.queue_free() # Supprime le tilemap
