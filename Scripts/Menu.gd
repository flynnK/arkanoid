extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	OS.center_window()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta :float) -> void:
	pass


func _input(event: InputEvent) -> void:
	var escape	= event.is_action_released("ui_cancel")
	var accept	= event.is_action_released("ui_accept")
	var pause	= event.is_action_released("key_p")
	
	if escape:
		get_tree().quit(0)
		# TODO : mettre une confirmation
	if accept:
		print("load scene Level01.tscn")
		#get_tree().change_scene_to(load("res://Scenes/Levels/Level.tscn"))
		var _value = get_tree().change_scene_to(load("res://Scenes/Game.tscn"))
	if pause:
		print("PAUSE")
	
