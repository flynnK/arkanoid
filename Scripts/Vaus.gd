extends KinematicBody2D

# Variables globales
var velocity: Vector2	= Vector2()
var speed: float 		= 180
var goBall: bool = false # Flag de start de la balle pour que la balle suive la raquette
var doMove: bool = false # Flag pour ne pas bouger la raquette

# Instancie la balle
onready var ball: KinematicBody2D = load("res://Scenes/Ball.tscn").instance()


# Fonctions génériques
func vaus_init() -> void:
#	var size = $Sprite.frames.get_frame("Default",0)
	# Centre la raquette horizontalement
	position = Vector2(104,240)
	
# Fonction GD

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Position les coordonnées de la raquette
	vaus_init()
	hide() # Cache la raquette
	$StartMusic.play(0)
#	$Balls.call_deferred("add_child", ball) # Ne pas utiliser $Ball.add_child
	$Balls.add_child(ball)
	ball.hide() # Cache la balle
	

func get_input() -> void:
	var right 	= Input.is_action_pressed("ui_right")
	var left	= Input.is_action_pressed("ui_left")
	var accept	= Input.is_action_pressed("ui_accept")
	
	if not goBall and accept:
		goBall = true
	var live = get_node("/root/Game/HUD/Lives")
	if live.get_child_count() > 0:
		velocity.x = 0
		velocity.y =0
	
		velocity.x=-int(left)+int(right)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	if doMove:
		get_input()
		# Mise à jour de la position de la raquette
		velocity.x = velocity.x*speed
		velocity = move_and_slide(velocity, Vector2.UP)

func _on_Sprite_animation_finished() -> void:
	match $Sprite.animation :
		"Start":
			$Sprite.play("Default")
			# Positionne la balle au dessus de la raquette
			ball.position.x = position.x
			ball.position.y = position.y-$Sprite.frames.get_frame("Default",0).get_height()
			ball.show() # Affiche la balle
			doMove = true # Autorise le déplacement de la raquette
		"Die":
			get_node("/root/Game").lostBall = true
			
func _on_StartMusic_finished() -> void:
	show() # Affiche la raquette
	$Sprite.play("Start")

